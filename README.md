IP_SERVER: XXX.XXX.XXX.XXX
PORT: 1337
getCoates [ru]
<Method for getting codes COATE.>
•	URL
<http://IP_SERVER:1337/api/v1/ru/coates>
•	Method:
GET {http-request-method} /api/]{protocol-version}/{language}/{serviceName}
	{http-request-method}: GET
	{protocol-version}: /v11
	{language}: ru
	{serviceName}: getCoates
•	URL Params
Required HTTP request headers:
	X-Road-Client: Specifies the member/subsystem that is used as a service client
	X-Road-Service: Specifies the serviceId that is invoked by the service client
	X-Road-Id: Unique identifier for this message
	X-Road-Request-Hash: For responses, this field contains sha-512 encoded hash of the request message
	X-Road-Error: This header is provided in case there was an error processing the request and it occurred somewhere in X-Road (on the consumer or provider Security Server)
	X-Road-Request-Id: Unique identifier for the request
	X-Road-UserId: 
Optional:
	X-Road-Issue:
•	Success Response:
HTTP status code:
200
Response body:
{
	"id": "1",
	"name": "Кыргызская Республика",
	"name1": "г.Бишкек",
	"code": "41700000000000"
 }
•	Error Response:
HTTP status code:
400
Response body:
{
    "type": "Client.BadRequest",
    "message": "Ошибка! Отсутствует заголовок: X-Road-Client"
}
HTTP status code:
500
Response body:
{
    "type": " Server.ServerProxy.DatabaseError ",
 	"message":"Error accessing database"
}
HTTP status code:
404 
Response body:
NOT FOUND